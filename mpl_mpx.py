from PyTango import DeviceProxy

import silx.third_party.EdfFile as EdfFile
import numpy as np
import time
import matplotlib.pyplot as plt
import matplotlib as mpl

dt = 0.02

# init pointers to the device servers
mpx_viewer = DeviceProxy("id01/liveviewer/mpx_1x4")
mpx_ccd = DeviceProxy("id01/limaccd/mpx_1x4")
shutter = DeviceProxy("id01/v-bsh/1")    

# load the latest mask
mask_edf = "/data/id01/inhouse/archive/setup/spatcorr-files/mpx4/mpx4_hotmask.edf"
mask = EdfFile.EdfFile(mask_edf).GetData(0)

def update():
	image  = mpx_viewer.read_attribute("Image").value
#		image *= mask	
	frame.set_data(image)
	fig.canvas.draw_idle()

# init figure
fig, ax = plt.subplots()

frame = ax.imshow(np.ones((516,516)), vmax=2)
plt.ion()
plt.show()

starttime = time.time()
count = 0
while True:
	# img_count is equal to
	# 0     : idle
	# -1    : counting
	# N > 0 : pscan 
	# this is the nr of the last img stored in the mpx memory
	img_count = mpx_ccd.read_attribute("video_last_image_counter").value 
	
	if img_count < 0: 
		print(count, img_count, '---', 'exposing')
		count = img_count
		pass
		
	elif img_count >= 0:
		print(count, img_count, '---', 'idle')
		if img_count > count:
			update()
			count = img_count
			print('--> updating img')
		else:
			pass

	fig.canvas.flush_events()
	time.sleep(dt - ((time.time() - starttime) % dt))


