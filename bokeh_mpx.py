import silx.third_party.EdfFile as EdfFile
import numpy as np
import time

from bokeh.plotting import figure, show, curdoc
from bokeh.models import ColumnDataSource, LogColorMapper, ColorBar

from tornado import gen
from threading import Thread
from PyTango import DeviceProxy

# specify polling time (seconds)
dt = 0.02

# init pointers to the MaxiPix device servers
mpx_viewer = DeviceProxy("id01/liveviewer/mpx_1x4")
mpx_ccd = DeviceProxy("id01/limaccd/mpx_1x4")
# shutter = DeviceProxy("id01/v-bsh/1")  # <-- use this to warn user for closed shutter

# load the latest detector mask
mask_edf = "/data/id01/inhouse/archive/setup/spatcorr-files/mpx4/mpx4_hotmask.edf"
mask = EdfFile.EdfFile(mask_edf).GetData(0)

# update the image plot
@gen.coroutine
def update():
    image = mpx_viewer.read_attribute("Image").value
    source.data["z0"] = [np.flipud(image)]

# trigger the update every N sec and only if
# the detector is ready to be polled
def monitor_mpx():
    starttime = time.time()
    count = 0
    while True:
        # img_count is equal to
        # 0     : idle
        # -1    : counting
        # N > 0 : continous scan (pscan)
        # img_count is the nr of the last img stored in the mpx memory
        img_count = mpx_ccd.read_attribute("video_last_image_counter").value

        if img_count < 0:
            # print(count, img_count, '---', 'exposing')
            count = img_count
            pass

        elif img_count >= 0:
            # print(count, img_count, '---', 'idle')
            if img_count > count:
                doc.add_next_tick_callback(update)
                count = img_count
                print("--> updating img")
            else:
                pass

        time.sleep(dt - ((time.time() - starttime) % dt))


# init data source
source = ColumnDataSource(data=dict(z0=[np.ones((516, 516))]))

# init colorbar
cmapper = LogColorMapper(palette="Viridis256", low=1)
cbar = ColorBar(color_mapper=cmapper, label_standoff=10)

# figure + image plot
fig = figure(tooltips=[("z", "@z0")], sizing_mode="scale_height")
frame = fig.image(
    image="z0", source=source, x=0, y=0, dw=516, dh=516, color_mapper=cmapper
)
fig.add_layout(cbar, "right")

# load the bokeh document
doc = curdoc()
doc.add_root(fig)

# launch the thread
thread = Thread(target=monitor_mpx)
thread.start()
